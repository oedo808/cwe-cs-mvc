﻿using System;
using System.Web;
namespace CsCoreMvcCwe.Models
{
    public static class CommonCleanser
    {
        public static String ToSafeString (String s)
        {
            return HttpUtility.HtmlEncode(s);
        }
    }
}
