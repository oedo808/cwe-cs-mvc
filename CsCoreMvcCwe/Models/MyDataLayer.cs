﻿using System;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlClient;

namespace CsCoreMvcCwe.Models
{
    public class MyDataLayer
    {
        public MyDataLayer()
        {
            
        }
        public static void QueryTable(DbConnectionStringBuilder connString, string sqlStatement)
        {
            using (SqlConnection conn = new SqlConnection(connString.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(sqlStatement, conn))
                {
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Console.WriteLine("{0} {1}", reader.GetString(0), reader.GetString(1));
                        }
                    }
                }
            }


        }
        public static void TryQueryTable(string tableName, string fieldName, DbConnectionStringBuilder connString)
        {
            string sqlStatement = "SELECT MAX(" + fieldName + ") FROM " + tableName;
            QueryTable(connString, sqlStatement);
        }
        public static void QuerySqlCommand(SqlCommand cmd, DbConnectionStringBuilder connString)
        {
            using (SqlConnection conn = new SqlConnection(connString.ConnectionString))
            {
                using (cmd)
                {
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Console.WriteLine("{0} {1}", reader.GetString(0), reader.GetString(1));
                        }
                    }
                }
            }
        }
    }
}
