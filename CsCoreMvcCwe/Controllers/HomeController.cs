﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Diagnostics;
using System.Security.Cryptography;
using System.IO;
using UtilityLibrary;
using System.Data.Common;
using System.Configuration;
using CsCoreMvcCwe.Models;
using System.Data.SqlClient;
using Veracode.Attributes;
//using FileUtils;

namespace CsCoreMvcCwe.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(String log, String log2)
        {
            log4net.Config.XmlConfigurator.Configure();
            log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            var mvcName = typeof(Controller).Assembly.GetName();
            var isMono = Type.GetType("Mono.Runtime") != null;

            ViewData["Version"] = mvcName.Version.Major + "." + mvcName.Version.Minor;
            ViewData["Runtime"] = isMono ? "Mono" : ".NET";
            String[] logArray = new string[2] { log, log2 };
            String inlineCleanLog = System.Net.WebUtility.HtmlEncode(log);
            logger.Warn(inlineCleanLog);
            Console.WriteLine(log);
            logger.Warn(log);
            String cleanLog = UtilityLibrary.StringUtilities.ToSafeString(log);
            logger.Warn(UtilityLibrary.StringUtilities.ToSafeString(log));
            logger.Warn(cleanLog);
            String cleanLog2 = CsCoreMvcCwe.Models.CommonCleanser.ToSafeString(log);
            logger.Warn(cleanLog2);
            logger.Warn(CsCoreMvcCwe.Models.CommonCleanser.ToSafeString(log));
            Console.WriteLine(HttpUtility.HtmlEncode(log));
            logger.Warn(HttpUtility.HtmlEncode(log));
            logger.Warn(secureLog(log));
            logger.Warn(toSecureArray(logArray));
            logger.Warn(secureLog(log) + toSecureArray(logArray));
            logger.Warn(HttpUtility.HtmlEncode(log).Replace("&#39;", "'").Replace("&quot;", "\"") + toSecureArray(logArray));
            logger.Warn(logArray);
            Console.WriteLine(HttpUtility.HtmlEncode(log).Replace("&#39;", "'").Replace("&quot;", "\""));
            logger.Warn(HttpUtility.HtmlEncode(log).Replace("&#39;", "'").Replace("&quot;", "\""));
            //logger.Warn(EncryptionTest(log).ToString());
            foreach(String encryptedText in EncryptionTest(log))
            {
                logger.Warn(encryptedText);
            }

            return View();
        }
        public ActionResult fileStuff(String filePath)
        {
            log4net.Config.XmlConfigurator.Configure();
            log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            logger.Warn(Path.GetFullPath(filePath));
            logger.Warn(Path.GetDirectoryName(filePath));

            return View();
        }
        public ActionResult connString (String tblName, String fieldName, String dbConnString)
        {
            DbConnectionStringBuilder dbcsb = new DbConnectionStringBuilder();
            dbcsb.ConnectionString = dbConnString;
            //dbcsb.ConnectionString = ConfigurationManager.ConnectionStrings["Helloworld"].ConnectionString;
            //DAO.TryQueryTable(tblName, fieldName, dbcsb);
            MyDataLayer.TryQueryTable(tblName, fieldName, dbcsb);
            //Class1.sqlQuery(tblName, fieldName, dbcsb);
            DAO.TryQueryTable(tblName, fieldName, dbcsb);

            return View();
        }
        public ActionResult paramTest (String a)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM test WHERE a=@simpleTest");
            cmd.Parameters.Add("@simpleTest", System.Data.SqlDbType.Int);
            cmd.Parameters["simpleTest"].Value = a;
            DbConnectionStringBuilder dbcsb = new DbConnectionStringBuilder();
            dbcsb.ConnectionString = ConfigurationManager.ConnectionStrings["Helloworld"].ConnectionString;
            MyDataLayer.QuerySqlCommand(cmd, dbcsb);
            return View();
        }
        private String secureLog(String taintedLog)
        {
            return HttpUtility.HtmlEncode(taintedLog);
        }
        private String[] toSecureArray(String[] taintedArray)
        {
            String[] cleansedArray = new String[taintedArray.Length];
            int count = 0;
            foreach(String element in taintedArray)
            {
                cleansedArray[count] = secureLog(element);
                count++;
            }
            return cleansedArray;
        }
        [FilePathCleanser]
        private List<string> EncryptionTest(string plainText)
        {
            List<string> encryptedStrings = new List<string>();
            byte[] encryptedCBC;
            byte[] encryptedCFB;
            byte[] encryptedCTS;
            byte[] encryptedECB;
            byte[] encryptedOFB;
            byte[] IV;
            var key = new byte[16];
            using (var random = new RNGCryptoServiceProvider())
            {
                random.GetBytes(key);
           
                
                using (Aes aesAlg = Aes.Create())
                {
                    aesAlg.Key = key;
                    aesAlg.GenerateIV();
                    aesAlg.Padding = PaddingMode.PKCS7;
                    IV = aesAlg.IV;
                    aesAlg.Mode = CipherMode.CBC;
                    var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (var swEncrypt = new StreamWriter(csEncrypt))
                            {
                                swEncrypt.Write(plainText);
                            }
                            encryptedCBC = msEncrypt.ToArray();
                        }
                    }
                    aesAlg.Mode = CipherMode.CFB;
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (var swEncrypt = new StreamWriter(csEncrypt))
                            {
                                swEncrypt.Write(plainText);
                            }
                            encryptedCFB = msEncrypt.ToArray();
                        }
                    }
                    aesAlg.Mode = CipherMode.CTS;
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (var swEncrypt = new StreamWriter(csEncrypt))
                            {
                                swEncrypt.Write(plainText);
                            }
                            encryptedCTS = msEncrypt.ToArray();
                        }
                    }
                    //aesAlg.Mode = CipherMode.ECB;
                    aesAlg.Mode = CipherMode.CBC;
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (var swEncrypt = new StreamWriter(csEncrypt))
                            {
                                swEncrypt.Write(plainText);
                            }
                            encryptedECB = msEncrypt.ToArray();
                        }
                    }
                    aesAlg.Mode = CipherMode.OFB;
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (var swEncrypt = new StreamWriter(csEncrypt))
                            {
                                swEncrypt.Write(plainText);
                            }
                            encryptedOFB = msEncrypt.ToArray();
                        }
                    }

                    encryptedStrings.Add(encryptedCBC.ToString());
                    encryptedStrings.Add(encryptedCFB.ToString());
                    encryptedStrings.Add(encryptedCTS.ToString());
                    encryptedStrings.Add(encryptedECB.ToString());
                    encryptedStrings.Add(encryptedOFB.ToString());
                   
                }
                return encryptedStrings;
            }
        }
    }
}
